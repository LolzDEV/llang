;;; llang-mode.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Lorenzo Torres
;;
;; Author: Lorenzo Torres <https://github.com/lolz>
;; Maintainer: Lorenzo Torres <lollotorres06@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(defconst llang-mode-syntax-table
  (with-syntax-table (copy-syntax-table)
    ;; C/C++ style comments
	(modify-syntax-entry ?/ ". 124b")
	(modify-syntax-entry ?* ". 23")
	(modify-syntax-entry ?\n "> b")
    ;; Chars are the same as strings
    (modify-syntax-entry ?' "\"")
    (syntax-table))
  "Syntax table for `llang-mode'.")

(eval-and-compile
  (defconst llang-keywords
    '("if" "else" "while" "do" "include" "memory" "proc" "const" "end" "offset" "reset" "assert" "in" "inline" "here" "put")))

(defconst porth-highlights
  `((,(regexp-opt llang-keywords 'symbols) . font-lock-keyword-face)))

;;;###autoload
(define-derived-mode llang-mode prog-mode "llang"
  "Major Mode for editing LLANG source code."
  (setq font-lock-defaults '(llang-highlights))
  (set-syntax-table llang-mode-syntax-table))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.lng\\'" . llang-mode))


(provide 'llang-mode)
;;; llang-mode.el ends here
