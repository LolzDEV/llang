# LLANG
LLang is a low level stack based programming language which can be compiled into native code

## Features
- [X] Compile to Linux x86_64 (nasm)
- [ ] Compile to Windows x86_64
- [ ] Compile to MacOS x86_64/aarch64
- [ ] Use LLVM IR
- [ ] Self hosted
- [X] Stack based
