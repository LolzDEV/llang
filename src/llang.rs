use std::io::Error;
use std::process::exit;

use colored::Colorize;

#[derive(Debug, Clone)]
pub enum Token {
    STRING(String),
    INT(i32),
    PLUS,
    EQUAL,
    GREATER,
    LESS,
    GreaterEqual,
    LessEqual,
    PUT,
    MINUS,
    STAR,
    IF(Option<u32>),
    END((Option<u32>, Option<u32>)),
    ELSE(Option<(u32, u32)>),
    DUP,
    WHILE(Option<u32>),
    DO(Option<u32>),
}

pub fn error(filename: &str, token: u32, msg: &str) {
    println!(
        "{} in {} at token {} -> {}",
        "[ERROR]".red(),
        filename,
        token,
        msg
    );
    exit(-1);
}

pub struct Parser;

impl Parser {
    pub fn parse(source: String) -> Vec<Token> {
        let mut tokens = Vec::new();
        let mut tok = String::new();
        let mut begin_string = false;
        let mut cur_string = String::new();

        let mut skip = false;

        for (i, c) in source.chars().enumerate() {
            if skip {
                skip = false;
                continue;
            }
            if c != ' ' {
                if c == '"' {
                    if begin_string {
                        tokens.push(Token::STRING(cur_string));
                        cur_string = String::new();
                    }
                    begin_string = !begin_string;
                } else if begin_string {
                    cur_string.push_str(&c.to_string());
                } else {
                    tok.push_str(&c.to_string());
                }
                if let Ok(num) = tok.trim().parse::<i32>() {
                    tokens.push(Token::INT(num));
                } else if tok == "+".to_string() {
                    tokens.push(Token::PLUS);
                } else if tok == "-".to_string() {
                    tokens.push(Token::MINUS);
                } else if tok == "=" {
                    tokens.push(Token::EQUAL);
                } else if tok == ">" {
                    if source.as_bytes()[i + 1] as char == '=' {
                        tokens.push(Token::GreaterEqual);
                        skip = true;
                    } else {
                        tokens.push(Token::GREATER);
                    }
                } else if tok == "<" {
                    if source.as_bytes()[i + 1] as char == '=' {
                        tokens.push(Token::LessEqual);
                        skip = true;
                    } else {
                        tokens.push(Token::LESS);
                    }
                } else if tok == "*".to_string() {
                    tokens.push(Token::STAR);
                } else if tok == "put".to_string() {
                    tokens.push(Token::PUT);
                } else if tok == "if" {
                    tokens.push(Token::IF(None));
                } else if tok == "end".to_string() {
                    tokens.push(Token::END((None, None)));
                } else if tok == "else" {
                    tokens.push(Token::ELSE(None));
                } else if tok == "dup" {
                    tokens.push(Token::DUP);
                } else if tok == "while" {
                    tokens.push(Token::WHILE(None));
                } else if tok == "do" {
                    tokens.push(Token::DO(None));
                }
            } else {
                tok = String::new();
            }
        }

        tokens
    }
}

pub struct Compiler {
    filename: String,
    strings: Vec<String>,
    stack_size: u32,
}

impl Compiler {
    pub fn new(filename: &str) -> Self {
        Self {
            filename: filename.to_string(),
            strings: Vec::new(),
            stack_size: 0,
        }
    }

    pub fn check_blocks(&self, tokens: &mut Vec<Token>) {
        let mut new_tokens = tokens.clone();
        for (i, tok) in tokens.iter().enumerate() {
            if let Token::IF(_) = tok {
                let mut counter = 1u32;
                let mut close_pos = i;
                while counter > 0 {
                    close_pos += 1;
                    if let Some(current_token) = tokens.get(close_pos) {
                        if let Token::IF(_) = current_token {
                            counter += 1;
                        } else if let Token::END(_) | Token::ELSE(_) = current_token {
                            counter -= 1;
                        }
                    } else {
                        error(&self.filename, i as u32, "missing closing delimiter");
                    }
                }

                new_tokens[i] = Token::IF(Some(close_pos as u32));
                new_tokens[close_pos] = Token::END((Some(close_pos as u32), None));
            } else if let Token::ELSE(_) = tok {
                let mut counter = 1u32;
                let mut close_pos = i;
                while counter > 0 {
                    close_pos += 1;
                    if let Some(current_token) = tokens.get(close_pos) {
                        if let Token::IF(_) | Token::ELSE(_) = current_token {
                            counter += 1;
                        } else if let Token::END(_) = current_token {
                            counter -= 1;
                        }
                    } else {
                        error(&self.filename, i as u32, "missing closing delimiter");
                    }
                }

                new_tokens[i] = Token::ELSE(Some((close_pos as u32, i as u32)));
                new_tokens[close_pos] = Token::END((Some(close_pos as u32), None));
            } else if let Token::WHILE(_) = tok {
                let mut counter = 1u32;
                let mut close_pos = i;
                while counter > 0 {
                    close_pos += 1;
                    if let Some(current_token) = tokens.get(close_pos) {
                        if let Token::WHILE(_) = current_token {
                            counter += 1;
                        } else if let Token::END(_) = current_token {
                            counter -= 1;
                        }
                    } else {
                        error(&self.filename, i as u32, "missing closing delimiter");
                    }
                }

                new_tokens[i] = Token::WHILE(Some(i as u32));
                new_tokens[close_pos] = Token::END((Some(close_pos as u32), Some(i as u32)));
            } else if let Token::DO(_) = tok {
                let mut counter = 1u32;
                let mut close_pos = i;
                while counter > 0 {
                    close_pos += 1;
                    if let Some(current_token) = new_tokens.get(close_pos) {
                        if let Token::DO(_) = current_token {
                            counter += 1;
                        } else if let Token::END((Some(_), Some(_))) = current_token {
                            counter -= 1;
                        }
                    } else {
                        error(&self.filename, i as u32, "missing closing delimiter");
                    }
                }

                new_tokens[i] = Token::DO(Some(close_pos as u32));
            }
        }

        *tokens = new_tokens;
    }

    pub fn compile(&mut self, tokens: &mut Vec<Token>) -> Result<String, Error> {
        let mut asm_result = String::new();

        asm_result.push_str("section .text\n");
        asm_result.push_str("\tglobal _start\n");
        asm_result.push_str("put:\n");
        asm_result.push_str("    mov     r9, -3689348814741910323\n");
        asm_result.push_str("    sub     rsp, 40\n");
        asm_result.push_str("    mov     BYTE [rsp+31], 10\n");
        asm_result.push_str("    lea     rcx, [rsp+30]\n");
        asm_result.push_str(".L2:\n");
        asm_result.push_str("    mov     rax, rdi\n");
        asm_result.push_str("    lea     r8, [rsp+32]\n");
        asm_result.push_str("    mul     r9\n");
        asm_result.push_str("    mov     rax, rdi\n");
        asm_result.push_str("    sub     r8, rcx\n");
        asm_result.push_str("    shr     rdx, 3\n");
        asm_result.push_str("    lea     rsi, [rdx+rdx*4]\n");
        asm_result.push_str("    add     rsi, rsi\n");
        asm_result.push_str("    sub     rax, rsi\n");
        asm_result.push_str("    add     eax, 48\n");
        asm_result.push_str("    mov     BYTE [rcx], al\n");
        asm_result.push_str("    mov     rax, rdi\n");
        asm_result.push_str("    mov     rdi, rdx\n");
        asm_result.push_str("    mov     rdx, rcx\n");
        asm_result.push_str("    sub     rcx, 1\n");
        asm_result.push_str("    cmp     rax, 9\n");
        asm_result.push_str("    ja      .L2\n");
        asm_result.push_str("    lea     rax, [rsp+32]\n");
        asm_result.push_str("    mov     edi, 1\n");
        asm_result.push_str("    sub     rdx, rax\n");
        asm_result.push_str("    xor     eax, eax\n");
        asm_result.push_str("    lea     rsi, [rsp+32+rdx]\n");
        asm_result.push_str("    mov     rdx, r8\n");
        asm_result.push_str("    mov     rax, 1\n");
        asm_result.push_str("    syscall\n");
        asm_result.push_str("    add     rsp, 40\n");
        asm_result.push_str("    ret\n");

        asm_result.push_str("_start:\n");

        for tok in tokens.iter() {
            match tok {
                Token::EQUAL => {
                    asm_result.push_str("\tmov rcx, qword 0\n");
                    asm_result.push_str("\tmov rdx, qword 1\n");
                    asm_result.push_str("\tpop rax\n");
                    asm_result.push_str("\tpop rbx\n");
                    asm_result.push_str("\tcmp rax, rbx\n");
                    asm_result.push_str("\tcmove rcx, rdx\n");
                    asm_result.push_str("\tpush rcx\n");
                }
                Token::GREATER => {
                    asm_result.push_str("\tmov rcx, qword 0\n");
                    asm_result.push_str("\tmov rdx, qword 1\n");
                    asm_result.push_str("\tpop rax\n");
                    asm_result.push_str("\tpop rbx\n");
                    asm_result.push_str("\tcmp rbx, rax\n");
                    asm_result.push_str("\tcmovg rcx, rdx\n");
                    asm_result.push_str("\tpush rcx\n");
                }
                Token::LESS => {
                    asm_result.push_str("\tmov rcx, qword 0\n");
                    asm_result.push_str("\tmov rdx, qword 1\n");
                    asm_result.push_str("\tpop rax\n");
                    asm_result.push_str("\tpop rbx\n");
                    asm_result.push_str("\tcmp rbx, rax\n");
                    asm_result.push_str("\tcmovl rcx, rdx\n");
                    asm_result.push_str("\tpush rcx\n");
                }
                Token::GreaterEqual => {
                    asm_result.push_str("\tmov rcx, qword 0\n");
                    asm_result.push_str("\tmov rdx, qword 1\n");
                    asm_result.push_str("\tpop rax\n");
                    asm_result.push_str("\tpop rbx\n");
                    asm_result.push_str("\tcmp rbx, rax\n");
                    asm_result.push_str("\tcmovge rcx, rdx\n");
                    asm_result.push_str("\tpush rcx\n");
                }
                Token::LessEqual => {
                    asm_result.push_str("\tmov rcx, qword 0\n");
                    asm_result.push_str("\tmov rdx, qword 1\n");
                    asm_result.push_str("\tpop rax\n");
                    asm_result.push_str("\tpop rbx\n");
                    asm_result.push_str("\tcmp rbx, rax\n");
                    asm_result.push_str("\tcmovle rcx, rdx\n");
                    asm_result.push_str("\tpush rcx\n");
                }
                Token::INT(num) => {
                    asm_result.push_str(&format!("\tpush {}\n", num));
                }
                Token::PLUS => {
                    asm_result.push_str("\tpop rax\n\tpop rbx\n\tadd rax, rbx\n\tpush rax\n");
                }
                Token::MINUS => {
                    asm_result.push_str("\tpop rax\n\tpop rbx\n\tsub rbx, rax\n\tpush rbx\n");
                }
                Token::STAR => {
                    asm_result.push_str("\tpop rax\n\tpop rbx\n\tmul rax, rbx\n\tpush rax\n");
                }
                Token::STRING(s) => {
                    asm_result.push_str(&format!(
                        "\tmov rax, {}\n\tmov [stack+{}], rax\n\tmov qword [stack+{}], str_{}\n",
                        self.stack_size,
                        self.stack_size + 1,
                        s.len(),
                        self.strings.len()
                    ));
                }
                Token::PUT => {
                    asm_result.push_str("\tpop rdi\n\tcall put\n");
                }
                Token::IF(Some(pos)) => {
                    asm_result.push_str("\tpop rax\n");
                    asm_result.push_str("\ttest rax, rax\n");
                    asm_result.push_str(&format!("\tjz addr_{}\n", pos));
                }
                Token::ELSE(Some(pos)) => {
                    asm_result.push_str(&format!("\tjmp addr_{}\n", pos.0));
                    asm_result.push_str(&format!("addr_{}:\n", pos.1));
                }
                Token::END((Some(pos), None)) => {
                    asm_result.push_str(&format!("addr_{}:\n", pos));
                }
                Token::END((Some(pos), Some(while_pos))) => {
                    asm_result.push_str(&format!("\tjmp addr_{}\n", while_pos));
                    asm_result.push_str(&format!("addr_{}:\n", pos));
                }
                Token::DUP => {
                    asm_result.push_str("\tpop rax\n");
                    asm_result.push_str("\tpush rax\n");
                    asm_result.push_str("\tpush rax\n");
                }
                Token::WHILE(Some(pos)) => {
                    asm_result.push_str(&format!("addr_{}:\n", pos));
                }
                Token::DO(Some(pos)) => {
                    asm_result.push_str("\tpop rax\n");
                    asm_result.push_str("\ttest rax, rax\n");
                    asm_result.push_str(&format!("\tjz addr_{}\n", pos));
                }
                _ => (),
            }
        }

        asm_result.push_str("\tmov rax, 60\n\tmov rdi, 0\n\tsyscall\n");

        asm_result.push_str("section .data\n");
        for (i, s) in self.strings.iter().enumerate() {
            asm_result.push_str(&format!("\tstr_{} db \"{}\", 0\n", i, s));
        }

        Ok(asm_result)
    }
}
