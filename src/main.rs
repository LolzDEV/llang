use colored::Colorize;
use llang::{Compiler, Parser};
use std::{
    env,
    fs::{self, File},
    io::Write,
    process::{exit, Command},
};

mod llang;

fn main() {
    let mut keep_asm = false;
    let mut debug = false;
    let mut filename = "";

    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("{}\nUsage: llang [ARGUMENTS] <filename>\nARGUMENTS:\n\t-a | --assembly: keep the generated assembly file.\n\t-g: generate debugging symbols.", "LLANG Compiler help page:".green());
        exit(-1);
    }

    for arg in args.iter() {
        if arg == "-h" || arg == "--help" {
            println!("{}\nUsage: llang [ARGUMENTS] <filename>\nARGUMENTS:\n\t-a | --assembly: keep the generated assembly file.\n\t-g: generate debugging symbols.", "LLANG Compiler help page:".green());
            exit(0);
        }
        if arg == "-a" || arg == "--assembly"{
            keep_asm = true;
        } else if arg == "-g" {
            debug = true;
        } else {
            filename = arg;
        }
    }

    let content = fs::read_to_string(filename).unwrap();

    let mut tokens = Parser::parse(content.trim().replace("\n", " ").to_string());

    let mut compiler = Compiler::new(&filename);

    compiler.check_blocks(&mut tokens);

    println!("{:?}", tokens);

    if let Ok(asm) = compiler.compile(&mut tokens) {
        println!("{} Creating assembly file.", "[INFO]".green().bold());
        if let Ok(mut file) = File::create("out.asm") {
            if let Err(_) = file.write_all(asm.as_bytes()) {
                println!(
                    "{} Failed to write the assembly file.",
                    "[ERROR]".red().bold()
                );
                exit(-1);
            }
        } else {
            println!(
                "{} Failed to create the assembly file.",
                "[ERROR]".red().bold()
            );
            exit(-1);
        }
    }

    println!("{} Assembling assembly file.", "[INFO]".green().bold());
    if let Err(_) = if !debug {
        Command::new("nasm")
            .arg("-f")
            .arg("elf64")
            .arg("out.asm")
            .output()
    } else {
        Command::new("nasm")
            .arg("-felf64")
            .arg("-Fdwarf")
            .arg("-g")
            .arg("out.asm")
            .output()
    } {
        println!(
            "{} Failed to assembling the assembly file.",
            "[ERROR]".red().bold()
        );
        exit(-1);
    }

    println!(
        "{} Linking object file to the executable.",
        "[INFO]".green().bold()
    );
    if let Err(_) = if !debug {
        Command::new("ld")
            .arg("-s")
            .arg("-o")
            .arg("output")
            .arg("out.o")
            .output()
    } else {
        Command::new("ld")
            .arg("-o")
            .arg("output")
            .arg("out.o")
            .output()
    } {
        println!(
            "{} Failed to link object file to the executable.",
            "[ERROR]".red().bold()
        );
        exit(-1);
    }

    if !keep_asm {
        println!("{} Removing assembly code.", "[INFO]".green().bold());
        if let Err(_) = Command::new("rm").arg("out.asm").output() {
            println!(
                "{} Failed to remove the assembly code.",
                "[ERROR]".red().bold()
            );
            exit(-1);
        }
    }

    println!("{} Removing object file.", "[INFO]".green().bold());
    if let Err(_) = Command::new("rm").arg("out.o").output() {
        println!(
            "{} Failed to remove the object file.",
            "[ERROR]".red().bold()
        );
        exit(-1);
    }
}
